<!-- 
	Patikrinti ar vartotojas prisijunges ir jo tipas admin
	Jei taip, isspausdinti vartotojo username
	Jei ne, redirect i klaidos puslapi
 -->

 <?php 
 	require('../db/session.php'); 

 	if($_SESSION['userType'] !== 1) {
		$_SESSION['error'] = 'Jūs neturite teisių matyti šio turinio!';
		header('Location: ../error.php');
	}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Administravimo panelė</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
</head>
<body>

	<?php

		if(!empty($_SESSION['isLoggedIn'])) :
			if($_SESSION['isLoggedIn']) : ?>

			<a href="../db/logout.php" class="btn btn-info">
				Atsijungti
			</a>

			<?php
			endif;
		endif;

	?>

	<h1>Sveiki atvyke, 
		<span>
			<?php echo $user['fullname']; ?>
		</span>
	</h1>

</body>
</html>