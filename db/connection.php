<?php

	// Prisijungimas prie DB
	define('_DBSERVER', 'localhost');
	define('_DBUSER', 'root');
	define('_DBPASS', '');
	define('_DBNAME', 'auto_db');

	try {
		// Atidarome prisijungima prie DB
		$conn = new PDO("mysql:host=" . _DBSERVER . ";dbname=" . _DBNAME . "", _DBUSER, _DBPASS);
		
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn->exec('set names utf8');

	} catch (PDOException $e) {
		echo "Connection failed: " . $e->getMessage();
	}
