<?php
	
	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AutoKCS</title>
	
	<!-- Bootstrap nuoroda -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Musu stilius -->
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
</head>
<body>

	<div class="container">
		<a href="auth/loginView.php" class="btn btn-warning">Prisijungti</a>
		<a href="auth/registerView.php" class="btn btn-danger">Registracija</a>

		<?php

			if(!empty($_SESSION['isLoggedIn'])) :
				if($_SESSION['isLoggedIn']) : ?>

				<a href="db/logout.php" class="btn btn-info">
					Atsijungti
				</a>

				<?php
				endif;
			endif;

		?>
	</div>


</body>
</html>



