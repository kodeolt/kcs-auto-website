<?php

	require('../../../db/connection.php');

	if(!empty($_POST['brand'])
		&& !empty($_POST['model'])
		&& !empty($_POST['year'])
		&& !empty($_POST['type'])
		&& !empty($_POST['engine'])
		&& !empty($_POST['user_id']))
	{
		// Gaunam duomenis is formos
		$brand 		= $_POST['brand'];
		$model 		= $_POST['model'];
		$year 		= $_POST['year'];
		$type 		= $_POST['type'];
		$engine 	= $_POST['engine'];
		$user_id 	= $_POST['user_id'];

		try {
			$sql = "
			INSERT INTO auto 
			(brand, model, year, user_id)
			VALUES
			('$brand', '$model', '$year', '$user_id')";

			if($conn->exec($sql)) {
				$last_id = $conn->lastInsertId();

				$sql = "INSERT INTO auto_data (type, engine, auto_id)
				VALUES('$type', '$engine', $last_id)";

				if($conn->exec($sql)) {
					header('Location: ../list/listView.php');
				}			
			}

			} catch(PDOException $e) {
				echo "Klaida: " . $e->getMessage();
			}

	} else {
		header('Location: ../../../index.php');
	}