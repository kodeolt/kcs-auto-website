<?php
	
	require('../../../db/session.php');

	$sql = "SELECT * FROM users u 
	INNER JOIN auto a ON u.id = a.user_id 
	INNER JOIN auto_data ad ON a.id = ad.auto_id
	WHERE u.id = " . $_SESSION['userId'] . " 
	ORDER BY a.brand ASC";

	$stmt = $conn->prepare($sql);
	$stmt->execute();

	$cars = $stmt->fetchAll();	

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AutoKCS</title>
	
	<!-- Bootstrap nuoroda -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Musu stilius -->
	<link rel="stylesheet" type="text/css" href="./assets/css/custom.css">
</head>
<body>

	<div class="container">
		<a href="../../user.php">Grįžti atgal </a>
		<h1>Jūsų skelbimai:</h1>

		<table class="table table-hover">
			<thead>
				<th>#</th>
				<th>Markė</th>
				<th>Modelis</th>
				<th>Metai</th>
				<th>Kėbulo tipas</th>
				<th>Variklio tūris</th>
			</thead>
			<tbody>
				<?php
				$counter = 1;
				foreach($cars as $c) : ?>
					<tr>
						<td><?php echo $counter++; ?></td>
						<td><?php echo $c['brand']; ?></td>
						<td><?php echo $c['model']; ?></td>
						<td><?php echo $c['year']; ?></td>
						<td><?php echo $c['type']; ?></td>
						<td><?php echo $c['engine']; ?></td>
					</tr>
				<?php
				endforeach;


				?>
			</tbody>
		</table>
	</div>


</body>
</html>



